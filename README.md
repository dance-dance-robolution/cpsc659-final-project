# CPSC 659 Fall '18 Final Project

Collaborators: Julia Lu, Roland Huang, Valerie Chen

## Detect the Beat

This package has the following Python dependencies (using pip install):
- opencv-python
- imutils
- pyaudio
- pygame

Run webcam.py with relevant flags to test the system. In order to play music, please add .mp3 files in the music/ folder named "60bpm.mp3" ... "140bpm.mp3" (incrementing by 10bpm, for a total of 9 files).

## Prior Work

1. Marynel Vazquez
	- https://gitlab.com/cpsc659-bim/assignments/f18-assignments/
2. Davy Wybiral
	- http://davywybiral.blogspot.com/2010/09/procedural-music-with-pyaudio-and-numpy.html
3. Adrian Rosebrock
	- https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/

