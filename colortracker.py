#!/usr/bin/env python

from collections import deque
import cv2
import imutils
import numpy as np
import sys
import time

import bpm_handler
from kalman_filter import KalmanFilter

def inner_angle(v1, v2):
    inner = np.dot(v1, v2) / (magnitude(v1) * magnitude(v2))
    return np.arccos(inner)

def magnitude(t):
    x, y = t
    return np.abs(np.square(x) + np.square(y))

class ColorTracker:

    def __init__(self, hsv_lower, hsv_upper, frame_width, frame_height, buffer_size, \
                    use_kalman, hide_tracker, tail_color):
        self.hsv_lower = hsv_lower
        self.hsv_upper = hsv_upper
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.buffer_size = buffer_size

        self.kf = None
        self.hide_tracker = hide_tracker
        if use_kalman:
            self.kf = KalmanFilter()
            self.kf_pts = deque(maxlen=buffer_size)
        self.tail_color = tail_color

        self.pts = deque(maxlen=buffer_size)
        # point positions are normalized to be between 0 and 1 in both dimensions
        self.normPts = deque(maxlen=buffer_size)
        # velocity and acceleration are normalized to account for fps
        self.vels = deque(maxlen=buffer_size)
        self.accs = deque(maxlen=buffer_size)

        self.lastcenter = None

    # input: original frame, frame converted to hsv colorspace, current real-time fps
    # Updates original frame with tracking graphics
    # return value: True if success, False otherwise
    def processFrame(self, frame, hsvframe, fps):
        mask = np.zeros(hsvframe.shape[:2], dtype=bool)
        for i in range(len(self.hsv_lower)):
            mask = mask | cv2.inRange(hsvframe, self.hsv_lower[i], self.hsv_upper[i])

        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        # debug color detection
        # if len(self.hsv_lower) == 1:
        #     cv2.imshow('Video', mask)

        conts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        conts = conts[0] if imutils.is_cv2() else conts[1]
        center = None

        if len(conts) > 0:
            c = max(conts, key=cv2.contourArea)
            ((x,y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            # skip duplicate frames
            if center == self.lastcenter:
                return False
            self.lastcenter = center

            if radius > 10 and not self.hide_tracker:
                cv2.circle(frame, (int(x), int(y)), int(radius), (0, 0, 255), 2)
                cv2.circle(frame, center, 5, (0, 0, 255), -1)

        # position
        self.pts.appendleft(center)
        if self.kf is not None:
            x = None if center is None else center[0]
            y = None if center is None else center[1]
            pred = self.kf.next_prediction(x, y)
            self.kf_pts.appendleft(pred)

        # determine which set of points to use to draw tail / calculate kinematics
        origin_pts = self.pts if self.kf is None else self.kf_pts

        # draw tail, with color based on mode
        if not self.hide_tracker:
            for i in range(1, len(origin_pts)):
                if origin_pts[i - 1] is None or origin_pts[i] is None:
                    continue
                thickness = int(np.sqrt(self.buffer_size / float(i+1)) * 2.5)
                cv2.line(frame, origin_pts[i-1], origin_pts[i], self.tail_color, thickness)

        # normalize if possible
        new_pt = origin_pts[0]
        if new_pt is None:
            self.normPts.appendleft(new_pt)
        else:
            self.normPts.appendleft((new_pt[0] / self.frame_width, new_pt[1] / self.frame_height))

        # velocity
        if len(self.normPts) >= 2 and self.normPts[0] is not None and self.normPts[1] is not None:
            v_x = self.normPts[0][0] - self.normPts[1][0]
            v_y = self.normPts[0][1] - self.normPts[1][1]
            # print(self.normPts[0], self.normPts[1])
            self.vels.appendleft((v_x * fps, v_y * fps))
            # print("VEL: {}".format(magnitude(self.vels[0])))

        # acceleration
        if len(self.vels) >= 2 and self.vels[0] is not None and self.vels[1] is not None:
            a_x = self.vels[0][0] - self.vels[1][0]
            a_y = self.vels[0][1] - self.vels[1][1]
            self.accs.appendleft((a_x * fps, a_y * fps))
            # print("Acc: {}".format(self.accs[0]))

        # if len(vels) >= 1 and len(self.accs) >= 1:
        #     print("{}".format(magnitude(self.accs[0])))

        return True

class SimpleColorTracker:

    def __init__(self, hsv_lower, hsv_upper, frame_width, frame_height, hide_tracker):
        self.hsv_lower = hsv_lower
        self.hsv_upper = hsv_upper
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.hide_tracker = hide_tracker
        self.lastcenter = None

    # purely tracks location -- for when beat detection is not needed
    def processFrame(self, frame, hsvframe, fps):
        mask = np.zeros(hsvframe.shape[:2], dtype=bool)
        for i in range(len(self.hsv_lower)):
            mask = mask | cv2.inRange(hsvframe, self.hsv_lower[i], self.hsv_upper[i])

        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        conts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        conts = conts[0] if imutils.is_cv2() else conts[1]
        center = None

        if len(conts) > 0:
            c = max(conts, key=cv2.contourArea)
            ((x,y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            self.lastcenter = center

            if radius > 10 and not self.hide_tracker:
                cv2.circle(frame, (int(x), int(y)), int(radius), (255, 0, 0), 2)
                cv2.circle(frame, center, 5, (255, 0, 0), -1)

        return center is not None
