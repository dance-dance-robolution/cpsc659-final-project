#!/usr/bin/env python

import argparse
from collections import deque
import cv2
import imutils
import numpy as np
import sys
import time
from tkinter import *

import threading
import queue
from threading import Thread
import time

import bpm_handler
from colortracker import *
from music_generator import MusicGenerator
from Window import Window

BUFFER_SIZE = 32
FRAMES_PER_FPS_SAMPLE = 30
MIN_FRAME_DIFFERENCE = 300000
MIN_SECS_BTWN_PULSES = 0.375 #correspods to 160 bpm
BLUE_COLOR = (255, 0, 0) 
GREEN_COLOR = (0, 255, 0)
RED_COLOR = (0, 0, 255)

def isPulse(pts, vels, accs):
    if len(accs) > 0 and magnitude(accs[0]) > 20 and magnitude(vels[0]) < 0.5: 
            #>50, <100 works decently ...  
            # and inner_angle(vels[0], vels[1]) > np.pi/6
        return True
    return False

def main(use_kalman, hide_tracker, music_gen, multi_tracking):

    video_capture = cv2.VideoCapture(0)
    # video_capture.set(cv2.CAP_PROP_FPS, 30) # standardize
    fps = video_capture.get(cv2.CAP_PROP_FPS)
    print('FPS: {}'.format(fps))

    if music_gen:
        mg = MusicGenerator() #if music_gen else None
        # used for communication with music generator thread
        speed_queue = queue.Queue()
        root_queue = queue.Queue(maxsize=1)
        last_root = 0    
        threading.Thread(target=play_notes, args=(speed_queue, root_queue, mg)).start()

    root = Tk()
    # root.geometry("1000x563")
    app = Window(root)
    root.update()

    frameHeight = video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
    frameWidth = video_capture.get(cv2.CAP_PROP_FRAME_WIDTH)
    aspectRatio = frameWidth / frameHeight
    print('Height x Width: {} x {}'.format(frameHeight, frameWidth))

    frameCounter = 0
    lastpulse_timestamp = -9999

    # good for yellow pom pom from CEID
    yellowLower = [(19, 150, 6)]
    yellowUpper = [(54, 255, 255)]

    # good for red pom pom from CEID
    redLower  = [(0, 180, 0), (170, 180, 6)]
    redUpper = [(10, 255, 255), (179, 255, 255)]
    pulse_pts = deque(maxlen=BUFFER_SIZE)
    yellowTracker = ColorTracker(yellowLower, yellowUpper, frameWidth, frameHeight, BUFFER_SIZE, \
                                    use_kalman, hide_tracker, RED_COLOR)
    # redTracker = ColorTracker(redLower, redUpper, frameWidth, frameHeight, BUFFER_SIZE, \
                                    # use_kalman, hide_tracker, BLUE_COLOR)
    redTracker = SimpleColorTracker(redLower, redUpper, frameWidth, frameHeight, hide_tracker)

    start = time.time()
    beat_count = 1

    out = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (int(frameWidth),int(frameHeight)))

    while True:
        # 720 x 1280 pixels
        # originally BGR: 720, 1280, 3
        ret, frame = video_capture.read()

        # frame = imutils.resize(frame, width=600)
        blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        hsvframe = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        success = yellowTracker.processFrame(frame, hsvframe, fps)
        # if multi_tracking:
        #     success = success and
        if not success:
            continue

        if multi_tracking and redTracker.processFrame(frame, hsvframe, fps):
            if redTracker.lastcenter is not None:
                index = 40 - int(redTracker.lastcenter[1] * 40.0 / frameHeight)
                if index != last_root:
                    root_queue.put(index)
                    # print("ADDED ROOT:", redTracker.lastcenter[1] / frameHeight)

        timestamp = time.time()
        # if pulse is found, add relevant point to queue of recent pulses
        if isPulse(yellowTracker.normPts, yellowTracker.vels, yellowTracker.accs) and timestamp - lastpulse_timestamp > MIN_SECS_BTWN_PULSES:
            bpm = bpm_handler.beat(timestamp) # current timestamp
            root.update()
            lastpulse_timestamp = timestamp
            new_pt = yellowTracker.kf_pts[0] if use_kalman else yellowTracker.pts[0]
            pulse_pts.appendleft(new_pt)
            if music_gen:
                if bpm != None:
                    speed_queue.put(60.0/bpm)
            else: 
                app.play_song_with_bpm(bpm)
            # print("BEAT: {}, BPM: {}".format(beat_count, bpm))
            beat_count = beat_count + 1
        else:
            pulse_pts.appendleft(None)

        # print recent pulses
        if not hide_tracker:
            for pt in pulse_pts:
                if pt is not None:
                    cv2.circle(frame, pt, 5, GREEN_COLOR, 5)

        # cv2.imshow('Video', frame)
        # out.write(frame)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        frameCounter += 1
        if frameCounter == FRAMES_PER_FPS_SAMPLE:
            end = time.time()
            fps = frameCounter / (end - start)
            frameCounter = 0
            # print('FPS: {}'.format(fps))
            start = time.time()

    video_capture.release()
    cv2.destroyAllWindows()

def play_notes(speed_queue, root_queue, mg):
    speed = 1
    root = 20 # range is [0, 40]
    while True:
        if not speed_queue.empty():
            speed = speed_queue.get(timeout=0)
        if not root_queue.empty():
            root = root_queue.get(timeout=0)
            print("UPDATED ROOT:", root)
        mg.randomBeat(speed, root)

if __name__ == '__main__':

    # command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--kalman', dest='use_kalman', action='store_true')
    parser.add_argument('--no-kalman', dest='use_kalman', action='store_false')
    parser.add_argument('--hide-tracker', dest='hide_tracker', action='store_true')
    # turns on option for generating music
    parser.add_argument('--music-gen', dest='music_gen', action='store_true')
    # turns on tracking for red
    parser.add_argument('--multi-tracking', dest='multi_tracking', action='store_true')
    parser.set_defaults(use_kalman=True, draw_tracker=False, music_gen=False, multi_tracking=False)
    args = parser.parse_args()

    main(args.use_kalman, args.hide_tracker, args.music_gen, args.multi_tracking)
    sys.exit(0)
